package ru.mcskill.launcher.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.settings.OS;

public class Data {

	private static File file = new File(Paths.get(OS.getConfigPath(), "config.properties").toString());
	private static File passFile = new File(Paths.get(OS.getConfigPath(), "password").toString());

	private static Properties change(String key, String value, Properties config) {
		config.setProperty(key, value);
		return config;
	}

	private static void save(Properties config) throws IOException {
		try (FileWriter f = new FileWriter(file)) {
			config.store(f, "Конфигурационный файл Менять что-то, только если знаете что");;
		} catch (IOException e) {
			if (Debug.isDebug())
				e.printStackTrace();
		}
	}

	public static void passSave(String pass, String key) {

		try (FileWriter f = new FileWriter(passFile)) {
			if (!passFile.exists())
				passFile.createNewFile();
			f.write(AES.encrypt(String.valueOf(pass), key));
		} catch (IOException | InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException
				| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e) {
			if (Debug.isDebug())
				e.printStackTrace();
		}
	}

	public static String getPass(String key) {
		String pass = null;

		try {
			pass = AES.decrypt(Files.readAllLines(passFile.toPath()).get(0), key);
		} catch (IOException | InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException
				| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e) {
			if (Debug.isDebug())
				e.printStackTrace();
		}

		return pass;
	}

	public static void deletePass() {
		passFile.delete();
	}
	
	private static Properties remove(Properties config, String key) {
		config.setProperty(key, "");
		return config;
	}

	public static void remove(String key) {
		try {
			save(remove(getConfig(), key));
		} catch (IOException e) {
			if (Debug.isDebug())
				e.printStackTrace();
		}	
	}

	public static void save(String key, String value) {
		try {
			save(change(key, value, getConfig()));
		} catch (IOException e) {
			if (Debug.isDebug())
				e.printStackTrace();
		}
	}

	public static void save(String key, int value) {
		try {
			save(change(key, String.valueOf(value), getConfig()));
		} catch (IOException e) {
			if (Debug.isDebug())
				e.printStackTrace();
		}
	}

	public static void save(String key, boolean value) {
		try {
			save(change(key, String.valueOf(value), getConfig()));
		} catch (IOException e) {
			if (Debug.isDebug())
				e.printStackTrace();
		}
	}

	public static int getInt(String key) {
		return Integer.parseInt(getConfig().getProperty(key).toString());
	}

	public static String getStr(String key) {
		return (String) getConfig().getProperty(key);
	}

	public static boolean getBool(String key) {
		return Boolean.parseBoolean(getConfig().getProperty(key).toString());
	}

	public static void setDefaultSettings() throws IOException {
		Properties config = new Properties();
		config.setProperty("authSave", "false");
		config.setProperty("mem", "512");
		config.setProperty("java", "java_x32");
		config.setProperty("debug", "false");

		save(config);

		log("Стандартный файл настроек сгенирирован.");
	}

	public static Properties getConfig() {
		Properties config = new Properties();
		
		if (!file.exists())
			try {
				setDefaultSettings();
			} catch (IOException e1) {
				if (Debug.isDebug())
					e1.printStackTrace();
			}
		
		try {
			config.load(new FileReader(file));
		} catch (IOException e) {
			log("ERROR: конфигурационный файл не найден");
		}

		return config;
	}

	private static void log(String what) {
		System.out.println("[Data] " + what);
	}
}

