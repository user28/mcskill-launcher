package ru.mcskill.launcher.ui;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import ru.mcskill.launcher.Launcher;
import ru.mcskill.launcher.clients.Clients;
import ru.mcskill.launcher.minecraft.Starter;
import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.util.Balance;
import ru.mcskill.launcher.util.News;
import ru.mcskill.launcher.util.Resource;

public class MainPane extends JComponent {

	private static final long serialVersionUID = 4551382614871743317L;
	private static JLabel progressBar = new JLabel("Загрузка: ");

	public MainPane(String login, char[] password) {

		setBackground(Color.GRAY);
		setSize(new Dimension(900, 600));

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(10, 523, 875, 40);
		add(panel_1);
		panel_1.setLayout(null);

		JButton bStartGame = new JButton("Играть");
		bStartGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setForeground(Color.BLACK);
			}
		});
		bStartGame.setForeground(Color.BLACK);
		bStartGame.setFont(new Font("Tahoma", Font.BOLD, 13));
		bStartGame.setContentAreaFilled(false);
		bStartGame.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				e.getComponent().setForeground(Color.LIGHT_GRAY);
			}
		});
		bStartGame.setFocusPainted(false);
		bStartGame.setBorderPainted(false);
		bStartGame.setBorder(null);
		bStartGame.setHorizontalTextPosition(SwingConstants.CENTER);
		bStartGame.setSize(new Dimension(100, 30));
		bStartGame.setBounds(new Rectangle(0, 0, 100, 30));
		bStartGame.setBounds(0, 1, 100, 39);
		panel_1.add(bStartGame);

		JButton bSettings = new JButton("Настройки");
		bSettings.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setForeground(Color.LIGHT_GRAY);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setForeground(Color.BLACK);
			}
		});
		bSettings.setForeground(Color.BLACK);
		bSettings.setHorizontalTextPosition(SwingConstants.CENTER);
		bSettings.setFont(new Font("Tahoma", Font.BOLD, 13));
		bSettings.setBorderPainted(false);
		bSettings.setBorder(null);
		bSettings.setFocusPainted(false);
		bSettings.setContentAreaFilled(false);
		bSettings.setSize(new Dimension(100, 30));
		bSettings.setBounds(new Rectangle(0, 0, 100, 30));
		bSettings.setBounds(110, 1, 100, 39);
		panel_1.add(bSettings);

		JButton bSite = new JButton("Сайт");
		bSite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Desktop desktop = Desktop.getDesktop();
				try {
					URI url = new URI("http://mcskill.ru");
					desktop.browse(url);
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		});
		bSite.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setForeground(Color.LIGHT_GRAY);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setForeground(Color.BLACK);
			}
		});
		bSite.setForeground(Color.BLACK);
		bSite.setHorizontalTextPosition(SwingConstants.CENTER);
		bSite.setFont(new Font("Tahoma", Font.BOLD, 13));
		bSite.setBorderPainted(false);
		bSite.setBorder(null);
		bSite.setFocusPainted(false);
		bSite.setContentAreaFilled(false);
		bSite.setSize(new Dimension(100, 30));
		bSite.setBounds(new Rectangle(0, 0, 100, 30));
		bSite.setBounds(220, 1, 100, 39);
		panel_1.add(bSite);

		JButton bForum = new JButton("Форум");
		bForum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Desktop desktop = Desktop.getDesktop();
				try {
					URI url = new URI("http://mcskill.ru/mcforum/");
					desktop.browse(url);
				} catch (URISyntaxException e1) {
					if (Debug.isDebug())
						e1.printStackTrace();
				} catch (IOException e2) {
					if (Debug.isDebug())
						e2.printStackTrace();
				}
			}
		});
		bForum.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setForeground(Color.LIGHT_GRAY);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setForeground(Color.BLACK);
			}
		});
		bForum.setForeground(Color.BLACK);
		bForum.setHorizontalTextPosition(SwingConstants.CENTER);
		bForum.setFont(new Font("Tahoma", Font.BOLD, 13));
		bForum.setBorderPainted(false);
		bForum.setBorder(null);
		bForum.setFocusPainted(false);
		bForum.setContentAreaFilled(false);
		bForum.setSize(new Dimension(100, 30));
		bForum.setBounds(new Rectangle(0, 0, 100, 30));
		bForum.setBounds(330, 1, 100, 39);
		panel_1.add(bForum);

		JButton blk = new JButton("Личный кабинет");
		blk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setForeground(Color.LIGHT_GRAY);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setForeground(Color.BLACK);
			}
		});
		blk.setForeground(Color.BLACK);
		blk.setHorizontalTextPosition(SwingConstants.CENTER);
		blk.setFont(new Font("Tahoma", Font.BOLD, 13));
		blk.setBorderPainted(false);
		blk.setBorder(null);
		blk.setFocusPainted(false);
		blk.setContentAreaFilled(false);
		blk.setSize(new Dimension(100, 30));
		blk.setBounds(new Rectangle(0, 0, 100, 30));
		blk.setBounds(440, 1, 150, 39);
		panel_1.add(blk);

		JButton bQuit = new JButton("Выйти");
		bQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Launcher.stop();
			}
		});
		bQuit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setForeground(Color.LIGHT_GRAY);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setForeground(Color.BLACK);
			}
		});
		bQuit.setForeground(Color.BLACK);
		bQuit.setHorizontalTextPosition(SwingConstants.CENTER);
		bQuit.setFont(new Font("Tahoma", Font.BOLD, 13));
		bQuit.setBorderPainted(false);
		bQuit.setBorder(null);
		bQuit.setFocusPainted(false);
		bQuit.setContentAreaFilled(false);
		bQuit.setSize(new Dimension(100, 30));
		bQuit.setBounds(new Rectangle(0, 0, 100, 30));
		bQuit.setBounds(775, 1, 100, 39);
		panel_1.add(bQuit);

		JEditorPane newsPane = new JEditorPane();
		newsPane.setFocusTraversalKeysEnabled(false);
		newsPane.setFocusable(false);
		newsPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		newsPane.setBackground(Color.WHITE);
		newsPane.setBounds(10, 52, 625, 460);
		newsPane.setContentType("text/html");
		newsPane.setEditable(false);
		try {
			newsPane.setPage(News.NEWS_URL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		add(newsPane);

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_3.setBackground(Color.WHITE);
		panel_3.setBounds(645, 52, 240, 460);
		add(panel_3);

		JLabel lblName = new JLabel("Привет, " + login);
		lblName.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblName.setBounds(10, 11, 240, 30);
		lblName.setForeground(Color.WHITE);
		add(lblName);

		JLabel lblMoney = new JLabel("Ваш баланс на сайте: " + Balance.getBalance(login));
		lblMoney.setForeground(Color.WHITE);
		lblMoney.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblMoney.setBounds(260, 11, 290, 30);
		add(lblMoney);

		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel<>(Clients.getAll().toArray()));
		comboBox.setBounds(705, 13, 180, 30);
		add(comboBox);

		JLabel lblNewLabel = new JLabel("Выбор клиента:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(525, 11, 170, 30);
		add(lblNewLabel);
		
		progressBar.setFont(new Font("Dialog", Font.BOLD, 14));		
		progressBar.setForeground(Color.WHITE);
		progressBar.setBounds(10, 563, 300, 25);
		progressBar.setVisible(false);
		add(progressBar);

		bSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LauncherFrame.getFrames()[0].setEnabled(false);
				new SettingsFrame();
			}
		});

		bStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Starter.start(comboBox.getSelectedItem().toString(), LauncherFrame.getFrames()[0]);
			}
		});
	}
	
	public static JLabel getProgressBar() {
		return progressBar;
	}

	public void paint(Graphics g) {
		g.drawImage(Resource.getMainBackgroundImage(), 0, 0, this);
		super.paint(g);
	}

	private void log(String what) {
		System.out.println("[MainPane] " + what);
	}
}
