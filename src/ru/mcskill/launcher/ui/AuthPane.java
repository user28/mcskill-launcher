package ru.mcskill.launcher.ui;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import ru.mcskill.launcher.auth.Authenticator;
import ru.mcskill.launcher.data.Data;
import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.util.Resource;

public class AuthPane extends JComponent {

	private static final Color DEFAULT_BUTTON_BACKGROUND_COLLOR = Color.WHITE;
	private static final Color DEFAULT_BUTTON_FOREGROUND_COLLOR = Color.BLACK;
	private static final long serialVersionUID = 735977558139769801L;

	public AuthPane(int x, int y, int width, int height, int i) {
		setBackground(Color.GRAY);

		JTextField loginField = new JTextField("");
		loginField.setName("");
		loginField.setBorder(new LineBorder(new Color(0, 0, 0)));
		loginField.setHorizontalAlignment(SwingConstants.CENTER);
		loginField.setBounds(x - width / 2, y, width, height);
		loginField.setText(Data.getStr("login"));
		add(loginField);

		JPasswordField passwordField = new JPasswordField();
		passwordField.setBorder(new LineBorder(new Color(0, 0, 0)));
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setBounds(x - width / 2, y + i + height, width, height);
		if (isAuthSave())
			passwordField.setText(Data.getPass(key(loginField.getText())));
		add(passwordField);

		JButton bLogin = new JButton("Войти");
		bLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setBackground(Color.LIGHT_GRAY);
				e.getComponent().setForeground(Color.WHITE);

			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setBackground(DEFAULT_BUTTON_BACKGROUND_COLLOR);
				e.getComponent().setForeground(DEFAULT_BUTTON_FOREGROUND_COLLOR);
			}
		});
		bLogin.setHorizontalTextPosition(SwingConstants.CENTER);
		bLogin.setFocusPainted(false);
		bLogin.setBounds(x - width / 2, y + (i * 2) + (height * 2), width, height);
		bLogin.setBorder(new LineBorder(new Color(0, 0, 0)));
		bLogin.setBackground(Color.white);
		add(bLogin);

		JButton bReg = new JButton("Нет аккаунта?");
		bReg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Desktop desktop = Desktop.getDesktop();
				try {
					URI url = new URI("http://mcskill.ru/mcforum/index.php?app=core&module=global&section=register");
					desktop.browse(url);
				} catch (URISyntaxException e1) {
					if (Debug.isDebug())
						e1.printStackTrace();
				} catch (IOException e2) {
					if (Debug.isDebug())
						e2.printStackTrace();
				}
			}
		});
		bReg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setForeground(Color.LIGHT_GRAY);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setForeground(Color.WHITE);
			}
		});
		bReg.setForeground(Color.WHITE);
		bReg.setFocusPainted(false);
		bReg.setBorderPainted(false);
		bReg.setContentAreaFilled(false);
		bReg.setBounds(x - width / 2, y * 2 + 135, width, height / 2);
		add(bReg);

		JButton bForgotPassword = new JButton("Забыл пароль?");
		bForgotPassword.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setForeground(Color.LIGHT_GRAY);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setForeground(Color.WHITE);
			}
		});
		bForgotPassword.setForeground(Color.WHITE);
		bForgotPassword.setFocusPainted(false);
		bForgotPassword.setContentAreaFilled(false);
		bForgotPassword.setBorderPainted(false);
		bForgotPassword.setBounds(x - width / 2, y * 2 + 120, width, height / 2);
		add(bForgotPassword);

		JCheckBox saveAuth = new JCheckBox("Запомнить меня");
		saveAuth.setOpaque(false);
		saveAuth.setForeground(Color.WHITE);
		saveAuth.setContentAreaFilled(false);
		saveAuth.setFocusPainted(false);
		saveAuth.setBackground(Color.BLACK);
		saveAuth.setBounds(x - width / 2 + width, y + i + height, width, height);
		saveAuth.setSelected(isAuthSave());
		add(saveAuth);

		bLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (new Authenticator().Auth(loginField.getText(), passwordField.getPassword())) {
					setVisible(false);
					getParent().add(new MainPane(loginField.getText(), passwordField.getPassword()));

					if (saveAuth.isSelected()) {
						Data.save("authSave", true);
						Data.save("login", loginField.getText());
						Data.passSave(String.valueOf(passwordField.getPassword()), key(loginField.getText()));
					} else {
						Data.save("authSave", false);
						Data.deletePass();
						Data.remove("login");
					}
				} else
					JOptionPane.showMessageDialog(bLogin, "Такой аккаунт не существует или вы указали неверный пароль",
							"Ошибка", JOptionPane.INFORMATION_MESSAGE);
			}
		});

//		if (isAuthSave())
//			next(bLogin);

		// log(Data.getPass(loginField.getText()));

	}

	private String key(String login) {
		char[] i = login.toCharArray();
		return String.valueOf(i.length);
	}

	private boolean isAuthSave() {
		return Data.getBool("authSave");
	}

	public void paint(Graphics g) {
		g.drawImage(Resource.getMainBackgroundImage(), 0, 0, this);
		super.paint(g);
	}
	
//	private static void next(JButton b) {
//		b.doClick();
//	}

	private void log(String what) {
		System.out.println("[AuthPane] " + what);
	}
}
