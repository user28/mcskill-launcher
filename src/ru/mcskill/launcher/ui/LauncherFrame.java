package ru.mcskill.launcher.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;

public class LauncherFrame extends JFrame{
	
	private static final long serialVersionUID = 8500982984954917274L;

	public LauncherFrame() {
//		setIconImage(Toolkit.getDefaultToolkit().getImage(LauncherFrame.class.getResource("/ru/mcskill/launcher/ui/background/bg.jpg")));
		this.setTitle("Лаунчер MCSKill.ru");
		this.getContentPane().setBackground(Color.GRAY);
		this.setVisible(true);
		this.setSize(new Dimension(900, 600));
		this.setResizable(false);
//		this.setLocation(OS.getResolution().width / 2 - this.getWidth() / 2, OS.getResolution().height / 2 - this.getHeight() / 2);
		this.setLocationRelativeTo(getOwner());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AuthPane authPane = new AuthPane(this.getWidth() / 2, this.getHeight() / 2 - 100, 200, 35, 12);
		authPane.setBackground(Color.GRAY);	
		
		this.getContentPane().add(authPane, BorderLayout.CENTER);
	}
	
	
	private void log(String what) {
		System.out.println("[LauncherFrame] " + what);
	}

}
