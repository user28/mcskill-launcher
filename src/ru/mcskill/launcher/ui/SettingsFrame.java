package ru.mcskill.launcher.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ru.mcskill.launcher.clients.Clients;
import ru.mcskill.launcher.data.Data;
import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.settings.OS;
import ru.mcskill.launcher.util.Downloader;
import ru.mcskill.launcher.util.Resource;

public class SettingsFrame extends JFrame {

	private static final long serialVersionUID = -3272588986480546612L;

	public SettingsFrame() {
		setBackground(Color.WHITE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				LauncherFrame.getFrames()[0].setEnabled(true);
				setVisible(false);
			}
		});
		getContentPane().setBackground(Color.GRAY);
		setTitle("Настройки");
		setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setSize(new Dimension(700, 250));
		this.setLocationRelativeTo(getParent());

		this.getContentPane().setLayout(null);

		JPanel panel = new JPanelExtended();
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 694, 250);
		getContentPane().add(panel);
		panel.setLayout(null);

		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBackground(Color.WHITE);
		comboBox.setBounds(257, 11, 200, 30);
		panel.add(comboBox);

		JLabel lblJava = new JLabel("Используемая Java:");
		lblJava.setForeground(Color.WHITE);
		lblJava.setBounds(77, 11, 170, 30);
		panel.add(lblJava);

		JSlider slider = new JSlider();
		slider.setBackground(Color.WHITE);
		// slider.setUI(new SliderUI(slider));
		slider.setSnapToTicks(true);
		slider.setMinorTickSpacing(256);
		slider.setMaximum((int) (OS.getPhysicalmemory() / (1024 * 1024)));
		slider.setMinimum(256);
		slider.setBounds(257, 52, 200, 26);
		slider.setValue(Data.getInt("mem"));
		panel.add(slider);

		JLabel lblmb = new JLabel("Выделенная память(MB): " + slider.getValue());
		lblmb.setForeground(Color.WHITE);
		lblmb.setBounds(27, 48, 220, 30);
		panel.add(lblmb);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 116, 250, 94);
		panel.add(panel_1);
		panel_1.setBackground(Color.WHITE);
		panel_1.setLayout(null);

		JButton bAuthClear = new JButton("");
		bAuthClear.setBackground(Color.GRAY);
		bAuthClear.setFocusPainted(false);
		bAuthClear.setBorder(new LineBorder(new Color(0, 0, 0)));
		bAuthClear.setBounds(10, 35, 45, 23);
		bAuthClear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Data.deletePass();
				Data.remove("login");
			}
		});
		panel_1.add(bAuthClear);

		JButton bJava = new JButton("");
		bJava.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				e.getComponent().setBackground(Color.LIGHT_GRAY);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				e.getComponent().setBackground(Color.GRAY);
			}
		});
		bJava.setBackground(Color.GRAY);
		bJava.setFocusPainted(false);
		bJava.setBorder(new LineBorder(new Color(0, 0, 0)));
		bJava.setBounds(10, 10, 45, 23);
		panel_1.add(bJava);

		JLabel label_1 = new JLabel("Очистить данные авторизации");
		label_1.setToolTipText("Будут удалены пароль и логин, в следующий раз вам придётся их вводить снова.");
		label_1.setBounds(57, 10, 193, 23);
		panel_1.add(label_1);

		JLabel label_3 = new JLabel("Загрузить заного Java_Portable");
		label_3.setToolTipText("Java_Portable(загружаемая лаунчером) будет удаленна и загружена снова.");
		label_3.setBounds(58, 35, 192, 23);
		panel_1.add(label_3);
		
		JCheckBox debug = new JCheckBox("Режим отладки");
		debug.setBounds(21, 62, 221, 23);
		debug.setContentAreaFilled(false);
		debug.setSelected(Debug.isDebug());
		
		panel_1.add(debug);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(272, 116, 230, 94);
		panel.add(panel_2);
		panel_2.setLayout(null);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(Clients.getAll().toArray()));
		comboBox_1.setBackground(Color.WHITE);
		comboBox_1.setBounds(10, 11, 210, 30);
		panel_2.add(comboBox_1);

		JButton bDel = new JButton("Удалить");
		bDel.setForeground(Color.WHITE);
		bDel.setBackground(Color.GRAY);
		bDel.setBorder(new LineBorder(new Color(0, 0, 0)));
		bDel.setFocusPainted(false);
		bDel.setBounds(131, 52, 89, 30);
		panel_2.add(bDel);

		JButton bRedownload = new JButton("Перекачать");
		bRedownload.setForeground(Color.WHITE);
		bRedownload.setBackground(Color.GRAY);
		bRedownload.setBorder(new LineBorder(new Color(0, 0, 0)));
		bRedownload.setFocusPainted(false);
		bRedownload.setBounds(10, 52, 111, 30);
		panel_2.add(bRedownload);
		bRedownload.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Downloader.load(comboBox_1.getSelectedItem().toString(), 0);
			}
		});
		
		JButton bSave = new JButton("Сохранить");
		bSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Data.save("mem", slider.getValue());
//				Data.save("java", comboBox.getSelectedItem().toString());
				Data.save("debug", debug.isSelected());
				
				LauncherFrame.getFrames()[0].setEnabled(true);
				setVisible(false);
			}
		});
		bSave.setForeground(Color.WHITE);
		bSave.setFocusPainted(false);
		bSave.setBorder(new LineBorder(new Color(0, 0, 0)));
		bSave.setBackground(Color.GRAY);
		bSave.setBounds(593, 180, 89, 30);
		panel.add(bSave);

		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblmb.setText("Выделенная память(MB): " + slider.getValue());
			}
		});
	}

	private static void log(String what) {
		System.out.println("[SettingsFrame] " + what);
	}
}

class JPanelExtended extends JPanel {

	private static final long serialVersionUID = -3024312222066412635L;

	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(Resource.getMainBackgroundImage(), -50, -50, null);
	}
}
