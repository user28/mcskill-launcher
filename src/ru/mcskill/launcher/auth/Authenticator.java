package ru.mcskill.launcher.auth;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.mcskill.launcher.settings.Debug;

public class Authenticator {
	private static String session;
	private static String login;

	// ссылка авторизации
	private static final String ROUTE_AUTHENTICATE = "http://launcher.mcskill.ru/auth.php";
	private final String USER_AGENT = "Mozilla/5.0";

	public boolean Auth(String login, char[] password) {
		try {
			String response = sendPost(login, password);
			if (!response.equals("Bad login")) {
				this.session = response.split(":")[1];
				this.login = response.split(":")[0];
				return true;
			}
		} catch (Exception e) {
			if (Debug.isDebug())
				e.printStackTrace();
		}

		return false;
	}

	private String sendPost(String login, char[] password) throws Exception {

		URL obj = new URL(ROUTE_AUTHENTICATE);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = "user=" + login + "&password=" + String.valueOf(password);

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		if (Debug.isDebug()) {
			log("\nSending 'POST' request to URL : " + ROUTE_AUTHENTICATE);
			// log("Post parameters : " + urlParameters);
			log("Response Code : " + responseCode);
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		if (Debug.isDebug())
			log(response.toString());
		return response.toString();

	}

	public static String getSession() {
		return session;
	}

	public static String getLogin() {
		return login;
	}
	
	private static void log(String what) {
		System.out.println("[AUTH] " + what);
	}

}
