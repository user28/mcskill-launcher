package ru.mcskill.launcher.clients;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.settings.OS;

/* Потом нужно переделать!! */

public class Clients {

	public static ArrayList<String> getAll() {
		ArrayList<String> clients = new ArrayList<>();
		clients.add("NewIndustrial_1.7.10");

		return clients;
	}

	public static boolean isDownloaded(String client) {
		File file = new File(Paths.get(OS.getRootDir(), client).toString());
		if (file.exists())
			return true;
		return false;
	}

	public static ArrayList<String> getDownloaded() {
		ArrayList<String> clients = new ArrayList<String>();
		for (String i : getAll()) {
			if (isDownloaded(i)) {
				clients.add(i);
			}
		}
		return clients;
	}

	public static String getLocalVersion(String client) {
		String version = null;
		File file = new File(Paths.get(OS.getRootDir(), client, "version.txt").toString());

		try {
			version = Files.readAllLines(file.toPath()).get(0);
		} catch (IOException e) {
			if (Debug.isDebug())
				e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Невозможно проверить версию установленного клиента.",
					"Ошибка", JOptionPane.INFORMATION_MESSAGE);
		}
		return version;
	}

	public static String getVersion(String client) {
		String version = null;
		try {
			URL oracle = new URL("http://s6.mcskill.ru/launcher/client/" + client + ".txt");
			BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

			version = in.readLine();
			in.close();
		} catch (Exception e) {
		}
		return version;
	}
}
