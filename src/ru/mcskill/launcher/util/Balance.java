package ru.mcskill.launcher.util;

public class Balance {
	
	public static  String getBalance(String login) {
		int balance = 1000;
		
		if (balance >= 10000) {
			balance /= 1000;
			return String.valueOf(balance) + "k";
		}		
		return String.valueOf(balance);
	}
	
	private static void log(String what) {
		System.out.println("[Balance] " + what);
	}
}
