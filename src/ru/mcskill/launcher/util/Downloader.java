package ru.mcskill.launcher.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

import ru.mcskill.launcher.clients.Clients;
import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.settings.OS;
import ru.mcskill.launcher.ui.MainPane;

public class Downloader {

	private static URL url;
	private static File file;

	public static void load(String client, int result) {
		try {
			if (result == 0) {
				file = new File(Paths.get(OS.getRootDir(), client + ".zip").toString());
				url = new URL(
						"http://s6.mcskill.ru/launcher/client/" + client + "_" + Clients.getVersion(client) + ".zip");
			} else {
				file = new File(Paths.get(OS.getRootDir(), client + "_mods" + ".zip").toString());
				url = new URL("http://s6.mcskill.ru/launcher/client/" + client + "_" + Clients.getVersion(client)
						+ "_mods" + ".zip");
			}
			if (file != null) {
				if (!file.exists()) {

					byte[] b = new byte[1024];

					new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								HttpURLConnection conn = (HttpURLConnection) Downloader.url.openConnection();
								BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
								FileOutputStream fw = new FileOutputStream(file);

								int count = 0;
								MainPane.getProgressBar().setVisible(true);
								while ((count = bis.read(b)) != -1) {
									fw.write(b, 0, count);
									MainPane.getProgressBar().setText("Загрузка: " + file.length() / (1024) + "kB / "
											+ conn.getContentLength() / (1024) + "kB");
								}
								MainPane.getProgressBar().setVisible(false);

								ZIP.extract(file.toString(), Paths.get(OS.getRootDir(), client).toString(), result);
								JOptionPane.showMessageDialog(null, "Загрузка завершена.", "Загрузка",
										JOptionPane.INFORMATION_MESSAGE);

								fw.close();
							} catch (IOException e) {
								if (Debug.isDebug())
									e.printStackTrace();
							}
						}
					}).start();
					
				}
			} else if (new File(Paths.get(OS.getRootDir(), client).toString()).exists()) {
				ZIP.extract(file.toString(), Paths.get(OS.getRootDir(), client).toString(), result);
			} else
				JOptionPane.showMessageDialog(null, "Что-то пошло не так.", "Ошибка", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			if (Debug.isDebug())
				e.printStackTrace();
		} 
	}
}
