package ru.mcskill.launcher.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import ru.mcskill.launcher.settings.Debug;

public class ZIP {

	public static void extract(String zipFile, String outputFolder, int result) {
		byte[] buffer = new byte[1024];

		try {

			File folder = new File(outputFolder);
			
			if (!folder.exists()) {
				folder.mkdir();
			} else if (folder.exists() && result == 0) {
				delete(folder);
				folder.mkdir();
			}

			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(Paths.get(outputFolder, fileName).toString());
				
				if (result != 0 && ze.isDirectory() && !newFile.getName().equals("saves")) {
					delete(newFile);
				}
				
				if (ze.isDirectory()) {
					newFile.mkdir();
					ze = zis.getNextEntry();
					continue;
				}
				if (Debug.isDebug())
					System.out.println("file unzip : " + newFile.getAbsoluteFile());
				
				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();
			
			new File(zipFile).delete();
			System.out.println("Распоковка закончена.");

		} catch (IOException ex) {
			ex.printStackTrace();
		}		
	}
	
	  private static void delete(File file)
	  {
	    if(!file.exists())
	      return;
	    if(file.isDirectory())
	    {
	      for(File f : file.listFiles())
	        delete(f);
	      file.delete();
	    }
	    else
	    {
	      file.delete();
	    }
	  }
}
