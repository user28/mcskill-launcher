package ru.mcskill.launcher.util;

import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.ui.LauncherFrame;

public class Resource {

	public static Image getMainBackgroundImage() {

		Image image = null;

		try {
			image = (Image) ImageIO.read(LauncherFrame.class.getResource("/ru/mcskill/launcher/ui/background/bg.jpg"));
		} catch (IOException e) {
			log("Изображение bg.jpg не найдено");
			if(Debug.isDebug())
				e.printStackTrace();
		}

		return image;
	}

	public static Image getSettingsBackgroundImage() {

		Image image = null;

		try {
			image = (Image) ImageIO
					.read(LauncherFrame.class.getResource("/ru/mcskill/launcher/ui/background/settingsbg.png"));
		} catch (IOException e) {
			log("Изображение settingsbg.png не найдено");
			if(Debug.isDebug())
				e.printStackTrace();
		}

		return image;
	}

	private static void log(String what) {
		System.out.println("[Loader] " + what);
	}
}
