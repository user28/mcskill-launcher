package ru.mcskill.launcher.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class News {
	
	public static final String NEWS_URL = "http://mcskill.ru/?page=news&rss";

	public static ArrayList<String> getNews() {
		try {
			URL oracle = new URL("http://mcskill.ru/?page=news&rss");
			BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

			String inputLine;
			ArrayList<String> news = new ArrayList();
			while ((inputLine = in.readLine()) != null)
				news.add(inputLine);
			in.close();
			return news;
		} catch (Exception e) {
		}
		
		return null;
	}
	
}
