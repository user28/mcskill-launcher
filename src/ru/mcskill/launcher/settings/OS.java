package ru.mcskill.launcher.settings;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import ru.mcskill.launcher.Launcher;

public class OS implements Runnable {
	
	private static long physicalMemory;
	private static Dimension resolution;
	private static Path rootDir;
	private static Path configPath;
	private static String OS;
	
	private static void mem() {
		com.sun.management.OperatingSystemMXBean os = (com.sun.management.OperatingSystemMXBean) java.lang.management.ManagementFactory
				.getOperatingSystemMXBean();
		physicalMemory = os.getTotalPhysicalMemorySize();
		log(String.valueOf(physicalMemory / (1024 * 1024)) + "MB");
	}

	private static void resolution() {
		resolution = Toolkit.getDefaultToolkit().getScreenSize();
		log("Resolution: " + resolution.width + "x" + resolution.height);
	}

	private static void dir() {
		if (OS.equals("Windows")) {
			rootDir = Paths.get(System.getProperty("user.home"), "appdata", "roaming", Launcher.getFolder());
		}

		if (OS.equals("Linux")) {
			rootDir = Paths.get(System.getProperty("user.home"), Launcher.getFolder());
		}

		configPath = Paths.get(rootDir.toString(), "launcher");
		log("ROOTDIR: " + rootDir.toString());
		log("CONFIG_PATH: " + configPath.toString());
	}

	private static void os() {
		String[] osName = System.getProperty("os.name").split(" ");
		OS = osName[0];
		log(OS);
	}

	public static void init() {
		os();
		mem();
		resolution();
		dir();
	}

	public static long getPhysicalmemory() {
		return physicalMemory;
	}

	public static Dimension getResolution() {
		return resolution;
	}

	public static String getRootDir() {
		return rootDir.toString();
	}

	public static String getOS() {
		return OS;
	}

	public static String getConfigPath() {
		return configPath.toString();
	}

	private static void log(String what) {
		System.out.println("[OS] " + what);
	}

	@Override
	public void run() {
		init();
	}

}
