package ru.mcskill.launcher;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.swing.SwingUtilities;

import ru.mcskill.launcher.data.Data;
import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.settings.OS;
import ru.mcskill.launcher.ui.LauncherFrame;

public class Launcher {

	// не забыть сменить!!!
	private static final String FOLDER = "mcskilltest";

	public static void main(String[] args) {
		
		OS.init();
		Launcher.init();
		
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new LauncherFrame();
			}
		});

	}

	private static void init() {
		File folder = new File(OS.getRootDir());
		File configFoler = new File(OS.getConfigPath());
		File config = new File(Paths.get(OS.getConfigPath(), "config.properties").toString());
		if (!folder.exists())
			folder.mkdir();
		if (!configFoler.exists()) {
			configFoler.mkdirs();
			try {
				config.createNewFile();
				Data.setDefaultSettings();
			} catch (IOException e) {
				if (Debug.isDebug())
					e.printStackTrace();
			}
		} else if (!config.exists()) {
			try {
				config.createNewFile();
				Data.setDefaultSettings();
			} catch (IOException e) {
				if (Debug.isDebug())
					e.printStackTrace();
			}
		} else if (config.exists()) {
			Data.getConfig();
		}
	}

	private static void log(String what) {
		System.out.println("[Launcher] " + what);

	}

	public static void stop() {
		System.exit(0);
	}

	public static String getFolder() {
		return FOLDER;
	}
}