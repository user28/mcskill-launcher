package ru.mcskill.launcher.minecraft;

import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.swing.JOptionPane;

import ru.mcskill.launcher.auth.Authenticator;
import ru.mcskill.launcher.clients.Clients;
import ru.mcskill.launcher.settings.Debug;
import ru.mcskill.launcher.settings.OS;
import ru.mcskill.launcher.util.Downloader;

public class Starter {

	public static void start(String client, Frame frame) {

		String command = "java -Dfile.encoding=UTF-8 -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.ignorePatchDiscrepancies=true -Djava.library.path="
				+ Paths.get(OS.getRootDir(), client, "natives") + " -cp "
				+ Paths.get(OS.getRootDir(), client, "bin", "*") + " net.minecraft.launchwrapper.Launch --username "
				+ Authenticator.getLogin() + " --gameDir " + Paths.get(OS.getRootDir(), client, "saves")
				+ " --assetIndex 1.7.10 --assetsDir " + Paths.get(OS.getRootDir(), client, "assets")
				+ " --version 1.7.10 --tweakClass com.mumfrey.liteloader.launch.LiteLoaderTweaker --tweakClass cpw.mods.fml.common.launcher.FMLTweaker --accessToken "
				+ Authenticator.getSession() + " --userProperties {}";
		
		if (!new File(Paths.get(OS.getRootDir(), client).toString()).exists()) {
			int result = JOptionPane.showOptionDialog(null, "Клиент не загружен, загрузить?", "Ошибка",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null,
					new Object[] { "Загрузить", "Отмена" }, "Да");
			if (result == JOptionPane.YES_OPTION) {
				Downloader.load(client, result);
			}
		}
			

		else if (Clients.getLocalVersion(client).equals(Clients.getVersion(client))) {
			frame.setVisible(false);
			try {
				Process process = Runtime.getRuntime().exec(command);
				Scanner kb = new Scanner(process.getInputStream());
				
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						while (process.isAlive()) {
							System.out.println("I'm alive");
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
							}
						}
						frame.setVisible(true);
					}
				}).start();
				
				if (Debug.isDebug())
					while (kb.hasNext()) {
						System.out.println(kb.nextLine());
					}
				

			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			int result = JOptionPane.showOptionDialog(null, "Для клиента доступно обновление, загрузить?", "Обновление",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null,
					new Object[] { "Обновить полностью", "Обновить только моды", "Отмена" }, "Да");
			if (result == JOptionPane.YES_OPTION || result == 1) {
				Downloader.load(client, result);
			}
		}

	}
}
